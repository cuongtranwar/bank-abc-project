package thirdparty.voucher.enums;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum VoucherStatus {
    SUCCESS, FAIED
}
