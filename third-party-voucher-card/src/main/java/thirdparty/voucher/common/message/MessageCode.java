package thirdparty.voucher.common.message;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum MessageCode {
    COMMON_INFO_001("common.info.001", "Your parameters are invalid."),
    COMMON_ERROR_500("common.error.500", "Internal Server Error"),
    COMMON_ERROR_204("common.error.204", " username or password is invalid." +
            " Please try again!");


    private String code;
    private String msg;

    MessageCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
