package thirdparty.voucher.service;

import thirdparty.voucher.dto.VoucherCardDto;

public interface VoucherService {

    String getVoucher(VoucherCardDto voucherCardDto);
}
