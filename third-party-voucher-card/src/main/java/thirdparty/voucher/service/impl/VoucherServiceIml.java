package thirdparty.voucher.service.impl;

import org.springframework.stereotype.Service;
import thirdparty.voucher.common.AuthException;
import thirdparty.voucher.common.message.MessageCode;
import thirdparty.voucher.dto.VoucherCardDto;
import thirdparty.voucher.service.VoucherService;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

@Service
public class VoucherServiceIml implements VoucherService {
    private final static Integer SLEEP_20_SECONDS = 20000;
    private final Random rand;

    public VoucherServiceIml() throws NoSuchAlgorithmException {
        rand = SecureRandom.getInstanceStrong();
    }

    @Override
    public String getVoucher(VoucherCardDto voucherCardDto) {
        int randomInt = rand.nextInt(4);
        switch (randomInt) {
            case 0:
                return "this is your voucher";
            case 1:
                try {
                    Thread.sleep(SLEEP_20_SECONDS);
                    return "this is your voucher";
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new AuthException(MessageCode.COMMON_ERROR_500);
                }
            case 2:
                Thread.currentThread().interrupt();
                throw new AuthException(MessageCode.COMMON_ERROR_500);
            default:
                return "this is your voucher";
        }
    }

}

