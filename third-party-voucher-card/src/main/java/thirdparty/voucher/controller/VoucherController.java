package thirdparty.voucher.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import thirdparty.voucher.dto.JsonResponse;
import thirdparty.voucher.dto.VoucherCardDto;
import thirdparty.voucher.service.VoucherService;


@RestController
@RequestMapping("/third-party")
public class VoucherController {


    private static final Logger logger = LoggerFactory.getLogger(VoucherController.class);
    private final VoucherService voucherService;

    public VoucherController(VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @PostMapping(value = "/voucher")
    public JsonResponse<String> login(@RequestBody VoucherCardDto voucherCardDto) {
        logger.info("Start Authentication method");
        return new JsonResponse<>(voucherService.getVoucher(voucherCardDto));
    }


}

