package thirdparty.voucher.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import thirdparty.voucher.enums.CardType;
import thirdparty.voucher.enums.VoucherStatus;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherCardDto {

    private String number;
    private CardType cardType;
    private String voucherCode;
    private String thirdPartyTokenRequest;

    private VoucherStatus voucherStatus;


    public VoucherCardDto(String thirdPartyTokenRequest) {
        this.thirdPartyTokenRequest = thirdPartyTokenRequest;
    }

    public VoucherCardDto(String number, CardType cardType, String voucherCode, VoucherStatus voucherStatus) {
        this.number = number;
        this.cardType = cardType;
        this.voucherCode = voucherCode;
        this.voucherStatus = voucherStatus;
    }
}
