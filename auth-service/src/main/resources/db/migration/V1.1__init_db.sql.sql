
CREATE TABLE IF NOT EXISTS user_profile (
  username                      varchar(40) not null,
  phone_number                  varchar(50) not null,
  first_name                    varchar(50) not null,
  last_name                     varchar(50) not null,
  password                      varchar(255) not null,
  email                         varchar(100),
  avatar                        varchar(255),
  created_date                  date not null,
  updated_date                  date not null,
  constraint pk_user_profile primary key (username)
);
INSERT INTO user_profile (`username`, `phone_number`,`first_name`,`last_name`,`password`,`email`,`created_date`,`updated_date`)
VALUES ("cuong.tran","0903528141", "Tran", "Cuong","7ae38b2e2c3d09466625b099491f93a889ea53275162cee698ccebe8ea8458256d76a2458946dcff","cuongtran.fetel@gmail.com",CURRENT_DATE ,CURRENT_DATE );