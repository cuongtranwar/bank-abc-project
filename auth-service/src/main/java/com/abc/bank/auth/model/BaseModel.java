package com.abc.bank.auth.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * <pre>
 * System Name : Messenger
 * Business Name : Base Java Persistent Object Model
 * Class Name : BaseModel
 * Description(ko) :
 * Description(en) : This class will define the Base Model, which has the common data fields
 * </pre>
 *
 * @author Linh Le
 * @since 2019.10.14
 */
@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseModel implements Serializable {

    @Column(nullable = false, updatable = false, name = "created_date")
    private Date createdDate;

    @Column(nullable = false, name = "updated_date")
    private Date updatedDate;

    /**
     * The handler for INSERT event
     */
    @PrePersist
    public void onPrePersist() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        setCreatedDate(now);
        setUpdatedDate(now);
    }

    /**
     * The handler for UPDATE event
     */
    @PreUpdate
    public void onPreUpdate() {
        setUpdatedDate(new Timestamp(System.currentTimeMillis()));
    }
}