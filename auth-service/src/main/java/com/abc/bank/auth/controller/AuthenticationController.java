package com.abc.bank.auth.controller;


import com.abc.bank.auth.common.dto.JsonResponse;
import com.abc.bank.auth.dto.LoginRequest;
import com.abc.bank.auth.dto.TokenJwtDto;
import com.abc.bank.auth.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
    private final AuthenticationService authenticationService;

    public AuthenticationController(
            AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping(value = "/login")
    public JsonResponse<TokenJwtDto> login(@RequestBody LoginRequest loginRequest) {
        logger.info("Start Login method");
        return new JsonResponse<>(authenticationService.login(loginRequest));
    }

}

