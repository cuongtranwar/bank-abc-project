package com.abc.bank.auth.service;


import com.abc.bank.auth.dto.LoginRequest;
import com.abc.bank.auth.dto.TokenJwtDto;

public interface AuthenticationService {

    /**
     * login authentication
     *
     * @param loginRequest
     * @return token if success
     */
    TokenJwtDto login(LoginRequest loginRequest);

    /**
     * logout authentication
     *
     * @param mobileDevice
     * @return true if success
     */
    Boolean logout(boolean mobileDevice);


}
