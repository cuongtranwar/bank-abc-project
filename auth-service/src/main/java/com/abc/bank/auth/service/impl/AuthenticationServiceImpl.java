package com.abc.bank.auth.service.impl;

import com.abc.bank.auth.common.SystemConstants;
import com.abc.bank.auth.common.exceptionhandling.AuthException;
import com.abc.bank.auth.common.message.MessageCode;
import com.abc.bank.auth.common.util.JwtTokenUtil;
import com.abc.bank.auth.dto.LoginRequest;
import com.abc.bank.auth.dto.TokenJwtDto;
import com.abc.bank.auth.model.UserModel;
import com.abc.bank.auth.repository.UserRepository;
import com.abc.bank.auth.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private static final Logger log = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    private final UserRepository userRepository;

    @Value("${jwt.secret-key}")
    private String secretKey;

    @Value("${password.secret-key}")
    private String passwordSecretKey;

    @Value("${jwt.expired-time}")
    private int expiredTimeInMillis;

    public AuthenticationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostConstruct
    private void init() {
        secretKey = JwtTokenUtil.encodeBase64(secretKey);
        passwordSecretKey = JwtTokenUtil.encodeBase64(passwordSecretKey);
    }

    @Override
    @Transactional
    public TokenJwtDto login(LoginRequest loginRequest) {

        UserModel userModel = userRepository.findByUserName(loginRequest.getUserName());
        if (Objects.isNull(userModel) || !verifyPassword(loginRequest.getPassword(), userModel.getPassword())) {
            throw new AuthException(MessageCode.COMMON_ERROR_204);
        }
        return generateTokenByUser(userModel);

    }


    @Override
    public Boolean logout(boolean mobileDevice) {
        return null;
    }


    private Map<String, Object> generateFinalClaims(String userName, String firstName, String lastName, String phone, String email) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(SystemConstants.USER_NAME, userName);
        claims.put(SystemConstants.PHONE, phone);
        claims.put(SystemConstants.FIRST_NAME, firstName);
        claims.put(SystemConstants.LAST_NAME, lastName);
        claims.put(SystemConstants.EMAIL, email);
        return claims;
    }

//    public static void main(String args[]) {
//        String passwordHash = hashPassword("123456");
//        System.out.println(passwordHash);
//        System.out.println(verifyPassword("123456", "51e217d526e4ec492ba289b35a8f64a3233ab57b996cecc72dd18872a8f3db72393dd05164772847"));
//    }
//
//    public static String hashPassword(String password) {
//        StandardPasswordEncoder encoder = new StandardPasswordEncoder("dGhpc2lzcGFzc3dvcmRzZWNyZXRrZXlzdHJpbmc=");
//        return encoder.encode(password);
//    }

    private TokenJwtDto generateTokenByUser(UserModel userJpo) {
        // Generate token
        Map<String, Object> claims = generateFinalClaims(
                userJpo.getUserName(),
                userJpo.getFirstName(),
                userJpo.getLastName(),
                userJpo.getPhoneNumber(),
                userJpo.getEmail()
        );
        String accessToken = JwtTokenUtil.generateToken(
                userJpo.getUserName(),
                claims,
                expiredTimeInMillis,
                secretKey
        );

        return new TokenJwtDto(accessToken);
    }

    private boolean verifyPassword(String drawpassword, String encodedPassword) {
        StandardPasswordEncoder encoder = new StandardPasswordEncoder(passwordSecretKey);
        return encoder.matches(drawpassword, encodedPassword);
    }


}