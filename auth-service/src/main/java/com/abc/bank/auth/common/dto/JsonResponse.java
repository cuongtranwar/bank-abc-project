package com.abc.bank.auth.common.dto;

import lombok.*;

import java.io.Serializable;


@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class JsonResponse<T> implements Serializable {

    private Boolean result = true;
    private String code = "200";
    private String message = "Success";

    private T data = null;

    public JsonResponse(T data) {

        this.data = data;
    }


}