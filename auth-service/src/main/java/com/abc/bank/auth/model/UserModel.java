package com.abc.bank.auth.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_profile")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserModel extends BaseModel {

    @Id
    @Column(length = 50, name = "username")
    private String userName;
    @Column
    private String password;
    @Column(length = 50, name = "phone_number")
    private String phoneNumber;
    @Column(length = 50, name = "first_name")
    private String firstName;
    @Column(length = 50, name = "last_name")
    private String lastName;
    @Column(length = 100)
    private String email;
    @Column(length = 100)
    private String avatar;

}