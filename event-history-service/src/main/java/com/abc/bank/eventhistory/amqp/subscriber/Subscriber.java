package com.abc.bank.eventhistory.amqp.subscriber;

import com.abc.bank.eventhistory.common.constant.RabbitmqConstant;
import com.abc.bank.eventhistory.dto.VoucherCardDto;
import com.abc.bank.eventhistory.model.VoucherSimCardEventModel;
import com.abc.bank.eventhistory.repository.VoucherSimCardEventRepository;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * <pre>
 * System Name : Messenger
 * Business Name :Subscriber Service
 * Class Name : Subscriber
 * Description(ko) :
 * Description(en) : This class will create consumer receive message from queue
 * </pre>
 */
@Service
public class Subscriber {
    private static final Logger log = LoggerFactory.getLogger(Subscriber.class);
    private static final String COUNT_RETRY = "count";
    private static final String INFO_MESSAGE_TO_QUEUE = "message move queue  {}";
    private final VoucherSimCardEventRepository voucherSimCardEventRepository;
    private final AmqpTemplate amqpTemplate;

    public Subscriber(VoucherSimCardEventRepository voucherSimCardEventRepository, AmqpTemplate amqpTemplate) {
        this.voucherSimCardEventRepository = voucherSimCardEventRepository;
        this.amqpTemplate = amqpTemplate;

    }

    @RabbitListener(queues = RabbitmqConstant.QUEUE_MESSAGE)
    @RabbitHandler
    @Transactional
    public void receivedMessage(Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag,
                                @Header(required = false, name = "x-death") List<HashMap> xDeath,
                                VoucherCardDto voucherCardDto) throws IOException {


        try {
            VoucherSimCardEventModel voucherSimCardEventModel = new VoucherSimCardEventModel(voucherCardDto);
            voucherSimCardEventRepository.save(voucherSimCardEventModel);
            channel.basicAck(tag, false);

        } catch (Exception ex) {
            if (!CollectionUtils.isEmpty(xDeath)) {
                if ((Long) xDeath.get(0).get(COUNT_RETRY) >= 3L) {
                    this.amqpTemplate.convertAndSend(RabbitmqConstant.EXCHANGE_WORK,
                            RabbitmqConstant.ROUTING_KEY_PARKING_LOT, voucherCardDto);
                    log.info(INFO_MESSAGE_TO_QUEUE,
                            RabbitmqConstant.PARKING_LOT_QUEUE_MESSAGE);
                    channel.basicAck(tag, false);
                } else {
                    channel.basicReject(tag, false);
                    log.info(INFO_MESSAGE_TO_QUEUE,
                            RabbitmqConstant.DEAD_LETTER_QUEUE_MESSAGE);
                }
            } else {
                channel.basicReject(tag, false);
                log.info(INFO_MESSAGE_TO_QUEUE,
                        RabbitmqConstant.DEAD_LETTER_QUEUE_MESSAGE);
            }
        }
    }


}
