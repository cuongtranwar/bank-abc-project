package com.abc.bank.eventhistory.service.impl;

import com.abc.bank.eventhistory.dto.VoucherCardDto;
import com.abc.bank.eventhistory.mapper.VoucherCardEventMapper;
import com.abc.bank.eventhistory.model.VoucherSimCardEventModel;
import com.abc.bank.eventhistory.repository.VoucherSimCardEventRepository;
import com.abc.bank.eventhistory.service.VoucherCardEventService;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VoucherCardEventServiceImpl implements VoucherCardEventService {
    private final VoucherSimCardEventRepository voucherSimCardEventRepository;
    private final VoucherCardEventMapper pricingChartMapper = Mappers.getMapper(VoucherCardEventMapper.class);

    public VoucherCardEventServiceImpl(VoucherSimCardEventRepository voucherSimCardEventRepository) {
        this.voucherSimCardEventRepository = voucherSimCardEventRepository;
    }

    @Override
    public List<VoucherCardDto> retrieveVoucherCard(String phoneNumber) {
        List<VoucherSimCardEventModel> voucherSimCardEventModels = voucherSimCardEventRepository.findByNumberPhone(phoneNumber);
        return voucherSimCardEventModels.stream().map(pricingChartMapper::toPricingDistrictMidPriceModel).collect(Collectors.toList());
    }
}
