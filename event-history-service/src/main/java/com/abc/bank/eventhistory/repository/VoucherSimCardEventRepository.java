package com.abc.bank.eventhistory.repository;

import com.abc.bank.eventhistory.model.VoucherSimCardEventModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoucherSimCardEventRepository extends MongoRepository<VoucherSimCardEventModel, Integer> {
    @Query("{ 'numberPhone' : ?0 }")
    List<VoucherSimCardEventModel> findByNumberPhone(String numberPhone);
}
