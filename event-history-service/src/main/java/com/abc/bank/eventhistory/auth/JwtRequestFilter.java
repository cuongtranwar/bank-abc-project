package com.abc.bank.eventhistory.auth;

import com.abc.bank.eventhistory.common.exceptionhandling.AuthException;
import com.abc.bank.eventhistory.common.message.MessageCode;
import com.abc.bank.eventhistory.common.util.JwtTokenUtil;
import com.abc.bank.eventhistory.dto.UserDetailDto;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Value("${jwt.secret-key}")
    private String secret;

    private String encodedSecret;

    @PostConstruct
    public void init() {
        encodedSecret = JwtTokenUtil.encodeBase64(secret);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        String jwtToken;

        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ") &&
                SecurityContextHolder.getContext().getAuthentication() == null) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                if (JwtTokenUtil.validateToken(jwtToken, encodedSecret)) {
                    UserDetailDto authenticatedUser = getUserFromJwt(jwtToken);

                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                            authenticatedUser, null, new ArrayList<>());


                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }

            } catch (ExpiredJwtException ex) {
                throw new AuthException(ex, MessageCode.COMMON_ERROR_500, HttpStatus.UNAUTHORIZED);
            }
        } else {
            logger.warn("JWT Token does not begin with Bearer String");
        }
        chain.doFilter(request, response);

    }

    private UserDetailDto getUserFromJwt(String jwtToken) throws IOException {
        String[] jwtPieces = jwtToken.split("\\.");
        String b64payload = jwtPieces[1];
        String jsonString = new String(Base64.decodeBase64(b64payload), StandardCharsets.UTF_8);
        //JSON from String to Object
        ObjectMapper mapper = new ObjectMapper();
        return mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(jsonString, UserDetailDto.class);
    }


}