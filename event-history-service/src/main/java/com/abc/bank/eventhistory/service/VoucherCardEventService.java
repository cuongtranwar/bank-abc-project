package com.abc.bank.eventhistory.service;

import com.abc.bank.eventhistory.dto.VoucherCardDto;

import java.util.List;

public interface VoucherCardEventService {


    /**
     * logout authentication
     *
     * @return true if success
     */
    List<VoucherCardDto> retrieveVoucherCard(String phoneNumber);


}
