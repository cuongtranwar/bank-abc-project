package com.abc.bank.eventhistory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventHistoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventHistoryApplication.class, args);
    }
}

