package com.abc.bank.eventhistory.amqp.config;

import com.abc.bank.eventhistory.common.constant.RabbitmqConstant;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * System Name : Messenger
 * Business Name :
 * Class Name : MessageQueueConfig
 * Description(ko) :
 * Description(en) : This class will create queue with exchange in rabbitmq
 * </pre>
 */
@Configuration
public class MessageQueueConfig {

    @Bean
    Queue queueMessage() {
        return QueueBuilder.durable(RabbitmqConstant.QUEUE_MESSAGE)
                .withArgument("x-dead-letter-exchange", RabbitmqConstant.EXCHANGE_RETRY)
                .withArgument("x-dead-letter-routing-key",
                        RabbitmqConstant.ROUTING_KEY_MESSAGE_DEAD_LETTER)
                .build();
    }

    @Bean
    Queue deadLetterQueue() {
        return QueueBuilder.durable(RabbitmqConstant.DEAD_LETTER_QUEUE_MESSAGE)
                .withArgument("x-dead-letter-exchange", RabbitmqConstant.EXCHANGE_WORK)
                .withArgument("x-dead-letter-routing-key", RabbitmqConstant.ROUTING_KEY_MESSAGE)
                .withArgument("x-message-ttl", 60000)
                .build();
    }

    @Bean
    Queue parkingLotQueue() {
        return QueueBuilder.durable(RabbitmqConstant.PARKING_LOT_QUEUE_MESSAGE)
                .build();
    }


    @Bean
    Binding bindingMessage(@Qualifier("queueMessage") Queue messageQueue,
                           @Qualifier("messageDirectExchange") DirectExchange messageDirectExchange) {

        return BindingBuilder.bind(messageQueue).to(messageDirectExchange)
                .with(RabbitmqConstant.ROUTING_KEY_MESSAGE);
    }

    @Bean
    Binding bindingDeadLetter(@Qualifier("deadLetterQueue") Queue deadLetterQueue,
                              @Qualifier("retryDirectExchange") DirectExchange retryDirectExchange) {

        return BindingBuilder.bind(deadLetterQueue).to(retryDirectExchange)
                .with(RabbitmqConstant.ROUTING_KEY_MESSAGE_DEAD_LETTER);
    }

    @Bean
    Binding bindingParkingLot(@Qualifier("parkingLotQueue") Queue parkingLotQueue,
                              @Qualifier("messageDirectExchange") DirectExchange messageDirectExchange) {

        return BindingBuilder.bind(parkingLotQueue).to(messageDirectExchange)
                .with(RabbitmqConstant.ROUTING_KEY_PARKING_LOT);
    }

}
