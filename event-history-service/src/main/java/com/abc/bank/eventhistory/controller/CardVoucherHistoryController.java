package com.abc.bank.eventhistory.controller;


import com.abc.bank.eventhistory.dto.JsonResponse;
import com.abc.bank.eventhistory.dto.UserDetailDto;
import com.abc.bank.eventhistory.dto.VoucherCardDto;
import com.abc.bank.eventhistory.service.VoucherCardEventService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/history")
public class CardVoucherHistoryController {

    private VoucherCardEventService voucherCardEventService;

    public CardVoucherHistoryController(VoucherCardEventService voucherCardEventService) {
        this.voucherCardEventService = voucherCardEventService;
    }

    /**
     * do login for user
     *
     * @return
     */
    @GetMapping(value = "/voucher")
    public JsonResponse<List<VoucherCardDto>> cardVoucher() {
        UserDetailDto userDetailDto = (UserDetailDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new JsonResponse<>(voucherCardEventService.retrieveVoucherCard(userDetailDto.getPhoneNumber()));
    }

}

