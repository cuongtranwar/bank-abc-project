package com.abc.bank.eventhistory.common.exceptionhandling;

import com.abc.bank.eventhistory.common.message.MessageCode;
import com.abc.bank.eventhistory.common.message.MessageInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Objects;

/**
 * <pre>
 * System Name : Landing Page
 * Business Name : Lannding Page Custom Exception
 * Class Name : AuthException
 * Description :
 * </pre>
 *
 * @author Cuong Tran
 * @since 2020.05.06
 */
@JsonIgnoreProperties({"cause", "stackTrace"})
@Getter
public class AuthException extends RuntimeException {

    /**
     * The logger
     */
    private static final Logger log = LoggerFactory.getLogger(AuthException.class);
    protected final HttpStatus httpStatus;
    protected final transient Object[] parameters;
    protected final MessageInfo messageInfo;

    /**
     * The constructor
     */
    public AuthException(MessageCode messageCode) {
        this(null, messageCode, null, null);
    }

    /**
     * The constructor
     *
     * @param cause
     * @param parameters
     */
    public AuthException(Throwable cause, MessageCode messageCode, HttpStatus httpStatus,
                         Object... parameters) {
        super(messageCode.getCode(), cause);
        this.httpStatus = httpStatus;
        this.parameters = parameters;
        this.messageInfo = buildMessageInfo(messageCode, parameters);
    }


    protected MessageInfo buildMessageInfo(MessageCode messageCode, Object... parameters) {
        MessageInfo info = new MessageInfo();
        try {
            info.setCode(messageCode.getCode());
            info.setMessage(String.format(messageCode.getMsg(), parameters));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return info;
    }

    @Override
    public String getMessage() {
        if (Objects.isNull(messageInfo)) {
            return super.getMessage();
        } else {
            return messageInfo.getMessage();
        }
    }
}
