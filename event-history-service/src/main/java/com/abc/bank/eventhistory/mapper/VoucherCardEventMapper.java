package com.abc.bank.eventhistory.mapper;


import com.abc.bank.eventhistory.dto.VoucherCardDto;
import com.abc.bank.eventhistory.model.VoucherSimCardEventModel;
import org.mapstruct.Mapper;

@Mapper
public interface VoucherCardEventMapper {

    VoucherCardDto toPricingDistrictMidPriceModel(VoucherSimCardEventModel p);
}
