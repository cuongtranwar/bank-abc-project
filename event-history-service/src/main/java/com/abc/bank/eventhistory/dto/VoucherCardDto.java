package com.abc.bank.eventhistory.dto;


import com.abc.bank.eventhistory.common.enums.CardType;
import com.abc.bank.eventhistory.common.enums.VoucherStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherCardDto {

    private String numberPhone;
    private CardType cardType;
    private String voucherCode;
    private String thirdPartyTokenRequest;

    private VoucherStatus voucherStatus;
    private LocalDateTime createdDate;


}
