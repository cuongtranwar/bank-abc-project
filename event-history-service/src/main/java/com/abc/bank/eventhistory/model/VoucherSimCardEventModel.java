package com.abc.bank.eventhistory.model;

import com.abc.bank.eventhistory.common.enums.CardType;
import com.abc.bank.eventhistory.common.enums.VoucherStatus;
import com.abc.bank.eventhistory.dto.VoucherCardDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoucherSimCardEventModel {
    private String numberPhone;
    private CardType cardType;
    private VoucherStatus voucherStatus;

    private LocalDateTime createdDate;


    public VoucherSimCardEventModel(VoucherCardDto voucherCardDto) {
        BeanUtils.copyProperties(voucherCardDto, this);
        this.createdDate = LocalDateTime.now();

    }
}
