package com.abc.bank.eventhistory.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * <pre>
 * System Name : Messenger
 * Business Name : Json Stringifier
 * Class Name : JsonStringifier
 * Description(ko) :
 * Description(en) : This class will provide utilities for stringify an object and vice versa
 * </pre>
 */
@NoArgsConstructor
public class JsonStringifier {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Stringify an object to string
     *
     * @param object
     * @return json string
     */
    public static String stringify(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return object.toString();
        }
    }

    /**
     * @param map   input map
     * @param clazz Class type of object to return
     * @param <T>   Type of object to return
     * @return Object of type {@code T}
     */
    public static <T> T convertMapToObject(Map map, Class<T> clazz) {
        return objectMapper.convertValue(map, clazz);
    }
}
