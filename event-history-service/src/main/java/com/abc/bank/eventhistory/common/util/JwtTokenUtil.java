package com.abc.bank.eventhistory.common.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JwtTokenUtil {

    public static String encodeBase64(String key) {
        return Base64.getEncoder().encodeToString(key.getBytes());
    }

    public static String generateToken(
            String subject,
            Map<String, Object> claims,
            int expiredTimeInMillis,
            String encodedBas64SecretKey) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(
                        System.currentTimeMillis() + expiredTimeInMillis))
                .signWith(SignatureAlgorithm.HS512, encodedBas64SecretKey)
                .compact();
    }

    //retrieve username from jwt token
    public static String getUsernameFromToken(String token, String encodedBas64SecretKey) {
        return getClaimFromToken(token, Claims::getSubject, encodedBas64SecretKey);
    }

    //retrieve username from jwt token
    public static String getClaimByClaimKey(String token, String claimKey, String encodedBas64SecretKey) {
        return getClaimFromToken(token, claimKey, encodedBas64SecretKey);
    }

    public static <T> T getClaimFromToken(String token, String key, String encodedBas64SecretKey) {
        final Claims claims = getAllClaimsFromToken(token, encodedBas64SecretKey);
        return (T) claims.get(key);
    }

    //retrieve expiration date from jwt token
    public static Date getExpirationDateFromToken(String token, String encodedBas64SecretKey) {
        return getClaimFromToken(token, Claims::getExpiration, encodedBas64SecretKey);
    }

    public static <T> T getClaimFromToken(String token,
                                          Function<Claims, T> claimsResolver,
                                          String encodedBas64SecretKey) {
        final Claims claims = getAllClaimsFromToken(token, encodedBas64SecretKey);
        return claimsResolver.apply(claims);
    }

    //for retrieveing any information from token we will need the secret key
    private static Claims getAllClaimsFromToken(String token, String encodedBas64SecretKey) {
        return Jwts.parser().setSigningKey(encodedBas64SecretKey).parseClaimsJws(token).getBody();
    }

    //check if the token has expired
    public static Boolean isTokenExpired(String token, String encodedBas64SecretKey) {
        final Date expiration = getExpirationDateFromToken(token, encodedBas64SecretKey);
        return expiration.before(new Date());
    }

    /**
     * Get User info from given token
     *
     * @return
     */

    public static String getUsername(String token, String encodedBas64SecretKey) {
        return Jwts.parser().setSigningKey(encodedBas64SecretKey).parseClaimsJws(token).getBody().getSubject();

    }

    /**
     * Get roles of user from given token
     *
     * @param token
     * @param encodedBas64SecretKey
     * @return
     */
    private static List<String> getClaimByKey(String token, String encodedBas64SecretKey, String claimKey) {
        return (List<String>) getAllClaimsFromToken(token, encodedBas64SecretKey).get(claimKey);
    }

    public static boolean validateToken(String authToken, String encodedBas64SecretKey) throws RuntimeException {
        Jwts.parser().setSigningKey(encodedBas64SecretKey).parseClaimsJws(authToken);
        return true;

    }

}