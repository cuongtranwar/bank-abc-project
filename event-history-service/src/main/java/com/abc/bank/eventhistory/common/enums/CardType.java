package com.abc.bank.eventhistory.common.enums;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum CardType {
    CARD_50K, CARD_100K, CARD_200K, CARD_500K
}
