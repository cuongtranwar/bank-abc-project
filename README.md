# Project Title

Bank ABC With Purchase Sim Card Voucher feature.

### Requirement
 For the testing, I just reduced the time Voucher Third Party respond is from **0s** to **20s** and the expected waiting time from our server is about **10s**.

### Prerequisites


```
1. docker-swarm
```

### Deployment

I already push service image to docker hub, 
so we just need deploy the docker swarm to run the system.

#### Step1

```
cd ./project-guide/deployment
```


```
docker stack deploy -c bank-abc-stack.yml bank-abc-stack
```

### Curl API 

#### 1. Login API

```
curl --location --request POST 'localhost/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userName" :"cuong.tran",
    "password": "123456"
}'
```

#### 2. Payment Process 
##### Note: - We assume the payment process is success so i do pseudo code for payment process to get payment token for the step Get Voucher Sim Card.
```
curl --location --request POST 'http://localhost/sim-card/payment' \
--header 'Content-Type: application/json' \
--data-raw '{
    "numberPhone": "0903528141",
    "cardType": "CARD_100K"
}'
```

#### 3. Get Voucher Card 
##### Param: 
* **tokenPayment**: This value get from Payment Process.

```$xslt
curl --location --request GET 'http://localhost/sim-card/voucher?tokenPayment=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwOTAzNTI4MTQxIiwibnVtYmVyUGhvbmUiOiIwOTAzNTI4MTQxIiwiY2FyZFR5cGUiOiJDQVJEXzEwMEsiLCJleHAiOjE1OTQzODA3NTgsImlhdCI6MTU5NDM4MDY1OH0.OQF-Dv_U6u6cEyYZgH-fnqHKTXkszgUQL5TmPAVNfWRxz5JEaJ4vuoOnaGeWQHtvzeJfGplKFX2cg8y0Dm_n_Q' \
```

#### 4. History Voucher
##### Header: 
* **Authorization**: Jwt Token, which return from Login
```
curl --location --request GET 'localhost/history/voucher' \
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJUcmFuIiwibGFzdE5hbWUiOiJDdW9uZyIsInN1YiI6ImN1b25nLnRyYW4iLCJwaG9uZU51bWJlciI6IjA5MDM1MjgxNDEiLCJleHAiOjE1OTQ0NjQyMTYsImlhdCI6MTU5NDM3NzgxNiwiZW1haWwiOiJjdW9uZ3RyYW4uZmV0ZWxAZ21haWwuY29tIiwidXNlcm5hbWUiOiJjdW9uZy50cmFuIn0.iJZzBzZlbTYtDwFnCeMmHS8t7QX-KX7AzNHSPfhVukf8C65JvhIGT6636q7ZUn1p8BqdBi5FijKNXchii7Emkg' \
```


### Architecture Design: 
#### Technical Layer:
 - **common**: includes some common project package such as util, exceptionhandling.
 - **config**: includes some common project package such as util, exceptionhandling.
 - **controller**: includes some common project package such as util, exceptionhandling.
 - **dto**: includes POJO Objects that communicate with client side.
 - **model**: includes Model Entity that communicate with database.
 - **repository**: includes implementation query/command to database.
 - **service**: includes bussiness logic of the application.

   
#### Microservice:
- **Auth-service:** For Authentication feature such as login, logout, refresh-token. I use MySQL to store user information.
- **Sim-card-service:** Integrate with Voucher third-party to get Sim Card Voucher. I use Redis to store paymentToken to make sure this request already did payment.
- **Event-History-service:** Collection Events from the others for tracking or monitoring. I use mongoDb for this service.

#### Core Lib, Framework:
- spring-boot : 2.2.2
- spring-data
- spring-security
- mapstruct
- lombok
- flywaydb

### Pattern And Principle: 
- **Observer Pattern**: Event History Service have a responsibility to listen change from RabbitMQ order to write logs.
- **Template Pattern**: In Sim Card Service, i implement simCardProcess() with **final** type. It's have some step must be followed.
We can integrate with the another third-party or the another event log service. But we need follow the requirement about **respond time**.
- **Jwt Token**: For validation between services together and for the authentication feature.
- **Solid Principle**: apply part of SOLID like Single responsibility, Interface segregation principle, Dependency inversion principle.
- **Component Principle**

### deployment package:
Includes **bank-abc-stack.yml** and nginx config file **default.conf**

### project-diagram package:
Includes Entity diagram and Architecture Design Diagram.


