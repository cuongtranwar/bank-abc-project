package com.abc.bank.simcard.common.exceptionhandling;

import com.abc.bank.simcard.common.message.MessageCode;
import com.abc.bank.simcard.common.message.MessageInfo;
import com.abc.bank.simcard.dto.JsonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Objects;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory
            .getLogger(RestResponseEntityExceptionHandler.class);
    private static final HttpHeaders headers = new HttpHeaders();

    public static JsonResponse<Object> getJsonResponseWithNoData(MessageInfo messageInfo) {
        JsonResponse<Object> response = new JsonResponse<>();
        response.setResult(false);
        response.setCode(messageInfo.getCode());
        response.setMessage(messageInfo.getMessage());
        response.setData(null);
        return response;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handleInvalidArgumentsException(WebRequest request,
                                                          ConstraintViolationException ex) {
        AuthException authException = new AuthException(MessageCode.COMMON_INFO_001);
        JsonResponse<Object> response =
                getJsonResponseWithNoData(authException.getMessageInfo());
        this.printLog(request, ex);
        return handleExceptionInternal(authException, response, headers, HttpStatus.OK, request);
    }

    @ExceptionHandler(AuthException.class)
    public ResponseEntity<Object> serviceRuntimeException(WebRequest request, AuthException ex) {
        JsonResponse<Object> response = getJsonResponseWithNoData(ex.getMessageInfo());

        this.printLog(request, ex);
        return Objects.nonNull(ex.getHttpStatus()) ? handleExceptionInternal(ex, response, headers,
                ex.getHttpStatus(), request) :
                handleExceptionInternal(ex, response, headers, HttpStatus.INTERNAL_SERVER_ERROR,
                        request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(WebRequest request, Exception ex) {
        AuthException authException = new AuthException(MessageCode.COMMON_ERROR_500);
        JsonResponse<Object> response = getJsonResponseWithNoData(authException.getMessageInfo());
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.printLog(request, ex);

        return
                handleExceptionInternal(ex, response, headers, status,
                        request);
    }

    private void printLog(WebRequest request, Exception e) {
        log.error("GlobalExceptionHandler error :: msg -> {} :: class -> {}",
                request.getContextPath(), e.getMessage(), e.getClass(), e);
    }
}
