package com.abc.bank.simcard.dto;


import com.abc.bank.simcard.common.enums.CardType;
import com.abc.bank.simcard.common.enums.VoucherStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherCardDto {

    private String numberPhone;
    private CardType cardType;
    private String voucherCode;
    private String thirdPartyTokenRequest;

    private VoucherStatus voucherStatus;
    private LocalDate createdDate;


    public VoucherCardDto(String thirdPartyTokenRequest) {
        this.thirdPartyTokenRequest = thirdPartyTokenRequest;
    }

    public VoucherCardDto(String number, CardType cardType, String voucherCode, VoucherStatus voucherStatus) {
        this.numberPhone = number;
        this.cardType = cardType;
        this.voucherCode = voucherCode;
        this.voucherStatus = voucherStatus;
    }
}
