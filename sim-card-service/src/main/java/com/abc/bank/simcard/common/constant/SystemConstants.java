package com.abc.bank.simcard.common.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SystemConstants {

    public static final String USER_NAME = "username";
    public static final String PHONE = "phoneNumber";
    public static final String FIRST_NAME = "firstName";
    public static final String EMAIL = "email";
    public static final String LAST_NAME = "lastName";
}
