package com.abc.bank.simcard.common.constant;

public class RabbitmqConstant {

    public static final String EXCHANGE_WORK = "exchange_work";
    public static final String EXCHANGE_RETRY = "exchange_retry";

    public static final String QUEUE_MESSAGE = "queue_message";

    public static final String PARKING_LOT_QUEUE_MESSAGE = "parking_lot_queue_message";

    public static final String ROUTING_KEY_MESSAGE = "message_routing_key";

    public static final String ROUTING_KEY_PARKING_LOT = "parking_lot_routing_key";

    public static final String ROUTING_KEY_MESSAGE_DEAD_LETTER = "dead_letter_routing_key";

    public static final String DEAD_LETTER_QUEUE_MESSAGE = "dead_letter_queue_message";


}
