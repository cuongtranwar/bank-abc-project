package com.abc.bank.simcard.service;


import com.abc.bank.simcard.common.enums.CardType;
import com.abc.bank.simcard.common.enums.VoucherStatus;
import com.abc.bank.simcard.common.exceptionhandling.AuthException;
import com.abc.bank.simcard.common.message.MessageCode;
import com.abc.bank.simcard.dto.VoucherCardDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public abstract class SimCardAbstract {
    private static final Integer SLEEP_10_SECONDS = 10000;

    protected abstract CompletableFuture<String> getSimCardFromThirdParty(String number, CardType cardType);

    protected abstract void syncSimCardEventToEventLog(String number, CardType cardType, String voucherCode, VoucherStatus voucherStatus);

    protected abstract void sendVoucherToSmsAndLog(CompletableFuture<String> asyncVoucher, String number, CardType cardType);

    protected abstract boolean isValidPaymentToken(String paymentToken);

    public final String simCardProcess(String paymentToken) throws InterruptedException, ExecutionException, IOException {
        if (isValidPaymentToken(paymentToken)) {

            VoucherCardDto voucherCardDto = getRequestFromJwt(paymentToken);

            CompletableFuture<String> stringCompletedFuture = getSimCardFromThirdParty(voucherCardDto.getNumberPhone(), voucherCardDto.getCardType());

            Thread.sleep(SLEEP_10_SECONDS);
            if (!stringCompletedFuture.isDone()) {
                sendVoucherToSmsAndLog(stringCompletedFuture, voucherCardDto.getNumberPhone(), voucherCardDto.getCardType());
                return "Your voucher card will be sent to SMS message";
            }

            if (stringCompletedFuture.isCompletedExceptionally()) {
                syncSimCardEventToEventLog(voucherCardDto.getNumberPhone(), voucherCardDto.getCardType(), null, VoucherStatus.FAIED);
                stringCompletedFuture.exceptionally(ex -> {
                    throw new AuthException(ex, MessageCode.COMMON_ERROR_500, HttpStatus.INTERNAL_SERVER_ERROR);
                });
            }

            syncSimCardEventToEventLog(voucherCardDto.getNumberPhone(), voucherCardDto.getCardType(), stringCompletedFuture.get(), VoucherStatus.SUCCESS);

            return stringCompletedFuture.get();
        } else {
            throw new AuthException(MessageCode.COMMON_ERROR_003);
        }

    }

    private VoucherCardDto getRequestFromJwt(String jwtToken) throws IOException {
        String[] jwtPieces = jwtToken.split("\\.");
        String b64payload = jwtPieces[1];
        String jsonString = new String(Base64.decodeBase64(b64payload), StandardCharsets.UTF_8);
        //JSON from String to Object
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonString, VoucherCardDto.class);
    }
}
