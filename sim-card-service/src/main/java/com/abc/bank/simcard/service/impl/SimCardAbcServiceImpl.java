package com.abc.bank.simcard.service.impl;


import com.abc.bank.simcard.amqp.publisher.Publisher;
import com.abc.bank.simcard.common.enums.CardType;
import com.abc.bank.simcard.common.enums.VoucherStatus;
import com.abc.bank.simcard.common.util.JwtTokenUtil;
import com.abc.bank.simcard.dto.JsonResponse;
import com.abc.bank.simcard.dto.VoucherCardDto;
import com.abc.bank.simcard.service.SimCardAbstract;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
public class SimCardAbcServiceImpl extends SimCardAbstract {
    private static final String PAYMENT_TOKEN_KEY = "payment-token";
    private final Publisher publisher;
    private final RedisTemplate redisTemplate;
    private final RestTemplate restTemplate;
    @Value("${sim-third-party-server.prefix}")
    private String prefix;
    @Value("${sim-third-party-server.endpoint-url}")
    private String loginEndpointUrl;
    @Value("${sim-third-party-server.secret-key}")
    private String thirdPartyVoucherSecretKey;
    @Value("${payment-server.secret-key}")
    private String paymentSecretKey;

    public SimCardAbcServiceImpl(Publisher publisher, RedisTemplate redisTemplate, RestTemplateBuilder restTemplateBuilder) {
        this.publisher = publisher;
        this.redisTemplate = redisTemplate;
        restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofMinutes(1L))
                .build();

    }

    @PostConstruct
    public void init() {
        paymentSecretKey = JwtTokenUtil.encodeBase64(paymentSecretKey);
        thirdPartyVoucherSecretKey = JwtTokenUtil.encodeBase64(thirdPartyVoucherSecretKey);
    }

    @Override
    protected CompletableFuture<String> getSimCardFromThirdParty(String number, CardType cardType) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("number", number);
        claims.put("cardType", cardType);

        String thirdPartyTokenRequest = JwtTokenUtil.generateToken(number, claims, 60000, thirdPartyVoucherSecretKey);
        return CompletableFuture.supplyAsync(() -> {
            HttpEntity<VoucherCardDto> entity = new HttpEntity<>(new VoucherCardDto(thirdPartyTokenRequest));
            JsonResponse jsonResponse =
                    restTemplate
                            .postForObject(prefix + loginEndpointUrl, entity,
                                    JsonResponse.class);
            return (String) Objects.requireNonNull(jsonResponse).getData();
        });
    }

    @Override
    protected void syncSimCardEventToEventLog(String number, CardType cardType, String voucherCode, VoucherStatus voucherStatus) {
        VoucherCardDto voucherCardDto = new VoucherCardDto(number, cardType, voucherCode, voucherStatus);
        publisher.produceMessage(voucherCardDto);
    }

    @Override
    protected void sendVoucherToSmsAndLog(CompletableFuture<String> asyncVoucher, String number, CardType cardType) {
        asyncVoucher.thenAcceptAsync(voucherCode -> {

            // TODO Call to SMS Third Party. Because the SMS Api is not free so I just put comment here.

            syncSimCardEventToEventLog(number, cardType, voucherCode, VoucherStatus.SUCCESS);
        }).exceptionally(ex -> {
            syncSimCardEventToEventLog(number, cardType, null, VoucherStatus.FAIED);
            return null;
        });
    }

    @Override
    protected boolean isValidPaymentToken(String paymentToken) {
        if (JwtTokenUtil.validateToken(paymentToken, paymentSecretKey) && !isFirstProcessWithAPaymentToken(paymentToken)) {
            savePaymentTokenInRedis(paymentToken);
            return true;
        }
        return false;
    }

    private void savePaymentTokenInRedis(String paymentToken) {
        redisTemplate.opsForHash().putIfAbsent(PAYMENT_TOKEN_KEY, paymentToken, StringUtils.EMPTY);


    }

    private boolean isFirstProcessWithAPaymentToken(String paymentToken) {
        return redisTemplate.opsForHash().hasKey(PAYMENT_TOKEN_KEY, paymentToken);
    }
}

