package com.abc.bank.simcard.amqp.publisher;

import com.abc.bank.simcard.common.constant.RabbitmqConstant;
import com.abc.bank.simcard.common.util.JsonStringifier;
import com.abc.bank.simcard.dto.VoucherCardDto;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.stereotype.Service;

@Service
public class Publisher {

    private final AmqpTemplate amqpTemplate;

    public Publisher(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    public void produceMessage(VoucherCardDto message) {
        this.amqpTemplate.convertAndSend(RabbitmqConstant.EXCHANGE_WORK,
                RabbitmqConstant.ROUTING_KEY_MESSAGE, buildJsonMessage(message));
    }


    private Message buildJsonMessage(VoucherCardDto instantMessageCmo) {
        String message = JsonStringifier.stringify(instantMessageCmo);

        return MessageBuilder.withBody(message.getBytes())
                .andProperties(
                        MessagePropertiesBuilder.newInstance().setContentType("application/json")
                                .build()).build();
    }


}
