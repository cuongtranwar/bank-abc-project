package com.abc.bank.simcard.common.util;

import com.abc.bank.simcard.common.exceptionhandling.AuthException;
import com.abc.bank.simcard.common.message.MessageCode;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Base64;
import java.util.Date;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JwtTokenUtil {

    public static String encodeBase64(String key) {
        return Base64.getEncoder().encodeToString(key.getBytes());
    }

    public static String generateToken(
            String subject,
            Map<String, Object> claims,
            int expiredTimeInMillis,
            String encodedBas64SecretKey) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(
                        System.currentTimeMillis() + expiredTimeInMillis))
                .signWith(SignatureAlgorithm.HS512, encodedBas64SecretKey)
                .compact();
    }

    public static boolean validateToken(String authToken, String encodedBas64SecretKey) {
        try {
            Jwts.parser().setSigningKey(encodedBas64SecretKey).parseClaimsJws(authToken);
            return true;
        } catch (Exception ex) {
            throw new AuthException(ex, MessageCode.COMMON_ERROR_002, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}