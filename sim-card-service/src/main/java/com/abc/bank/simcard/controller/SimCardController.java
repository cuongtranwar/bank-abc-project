package com.abc.bank.simcard.controller;


import com.abc.bank.simcard.common.util.JwtTokenUtil;
import com.abc.bank.simcard.dto.JsonResponse;
import com.abc.bank.simcard.dto.VoucherCardDto;
import com.abc.bank.simcard.service.impl.SimCardAbcServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping("/sim-card")
public class SimCardController {

    private final SimCardAbcServiceImpl simCardAbcService;

    public SimCardController(SimCardAbcServiceImpl simCardAbcService) {
        this.simCardAbcService = simCardAbcService;
    }

    @GetMapping(value = "/voucher")
    public JsonResponse<String> voucher(@RequestParam(required = false) String tokenPayment) throws ExecutionException, InterruptedException, IOException {

        return new JsonResponse<>(simCardAbcService.simCardProcess(tokenPayment));
    }

    /**
     * This one is pseudo code for payment process. It should in Payment Service.
     *
     * @param voucherCardDto
     * @return paymentToken
     */
    @PostMapping(value = "/payment")
    public JsonResponse<String> payment(@RequestBody VoucherCardDto voucherCardDto) {
        // TODO We assume the payment process is success so i do pseudo code for payment process to get payment token for the step Get Voucher Sim Card.
        Map<String, Object> claims = new HashMap<>();
        claims.put("numberPhone", voucherCardDto.getNumberPhone());
        claims.put("cardType", voucherCardDto.getCardType());
        String tokenPayment = JwtTokenUtil.generateToken(voucherCardDto.getNumberPhone(), claims, 100000,
                JwtTokenUtil.encodeBase64("thisispaymentserversecretkey"));

        return new JsonResponse<>(tokenPayment);
    }


}

