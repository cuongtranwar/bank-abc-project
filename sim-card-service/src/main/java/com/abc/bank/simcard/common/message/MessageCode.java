package com.abc.bank.simcard.common.message;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum MessageCode {
    COMMON_INFO_001("common.info.001", "Your parameters are invalid."),
    COMMON_ERROR_500("common.error.500", "Internal Server Error"),
    COMMON_ERROR_204("common.error.204", " username or password is invalid." +
            " Please try again!"),
    COMMON_ERROR_003("common.error.003", "This Payment token is already used."),

    COMMON_ERROR_002("common.error.002", "Jwt Token Is invalid");


    private String code;
    private String msg;

    MessageCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
