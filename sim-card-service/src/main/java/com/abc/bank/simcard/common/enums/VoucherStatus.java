package com.abc.bank.simcard.common.enums;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum VoucherStatus {
    SUCCESS, FAIED
}
